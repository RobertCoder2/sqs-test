<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use Aws\Sqs\SqsClient;
use Aws\Exception\AwsException;

class AddToQueueCommand extends Command
{
    protected static $defaultName = 'app:add-to-queue';
    protected static $defaultDescription = 'Add a short description for your command';

    private $key = '';
    private $secret = '';

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $client = new SqsClient([
            'credentials' => [
                'key'    => $this->key,
                'secret' => $this->secret,
            ],
            'region' => 'eu-west-1',
            'version' => '2012-11-05'
        ]);

        $theMessage = json_encode([
            'email' => 'robert.staats@dpgmedia.nl',
            'name'  => 'Robert',
            'phone' => '0123456789'
        ]);

        $params = [
            'MessageGroupId' => 'user-service-v2',
            'MessageDeduplicationId' => md5($theMessage),
            'DelaySeconds' => 0,
            'MessageBody' => $theMessage,
            'QueueUrl' => 'https://sqs.eu-west-1.amazonaws.com/218610432265/MyQueue.fifo'
        ];

        try {
            $result = $client->sendMessage($params);
            var_dump($result);
        } catch (AwsException $e) {
            error_log($e->getMessage());
        }

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return 0;
    }
}
