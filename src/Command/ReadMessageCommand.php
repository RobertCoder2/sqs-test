<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use Aws\Sqs\SqsClient;
use Aws\Exception\AwsException;
use Aws\Sns\SnsClient;

use App\Entity\People;

class ReadMessageCommand extends Command
{
    protected static $defaultName = 'app:read-message';
    protected static $defaultDescription = 'Add a short description for your command';

    private $entityManager;

    private $key = '';
    private $secret = '';

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        //$queueUrl = 'https://sqs.eu-west-1.amazonaws.com/218610432265/roberts-fifo-queue.fifo';
        $queueUrl = 'https://sqs.eu-west-1.amazonaws.com/218610432265/MyQueue.fifo';

        $client = new SqsClient([
            'credentials' => [
                'key'    => $this->key,
                'secret' => $this->secret,
            ],
            'region' => 'eu-west-1',
            'version' => '2012-11-05'
        ]);

        try {
            $result = $client->receiveMessage(array(
                'AttributeNames' => ['SentTimestamp'],
                'MaxNumberOfMessages' => 1,
                'MessageAttributeNames' => ['All'],
                'QueueUrl' => $queueUrl, // REQUIRED
                'WaitTimeSeconds' => 0,
            ));
            if (!empty($result->get('Messages'))) {
                $data =  json_decode( $result->get('Messages')[0]['Body'] ?? [], true );
                if ($data) {

                    $person = new People();
                    $person->setEmail($data['email'] ?? '' );
                    $person->setName($data['name'] ?? '' );
                    $person->setPhone($data['phone'] ?? '' );
                    $person->setRegistered(new \DateTime());

                    $this->entityManager->persist($person);
                    $this->entityManager->flush();

                    $io->success('User added.');
                    $this->publishMessage($data);

                    var_dump($data);

                } else {
                    $io->success('Bad data on queue.');
                    var_dump($result->get('Messages'));
                }
                // remove item from queue
                $client->deleteMessage([
                    'QueueUrl' => $queueUrl, // REQUIRED
                    'ReceiptHandle' => $result->get('Messages')[0]['ReceiptHandle'] // REQUIRED
                ]);
            } else {
                $io->success('No messages in queue.');
            }
        } catch (AwsException $e) {
            $io->error($e->getMessage());
        }

        return 0;
    }

    public function publishMessage($data)
    {
        $message = 'This message is sent from a Amazon SNS code sample. Door Nino Dus: <br> <br>Data of user update: <br> <br>' . json_encode($data, JSON_PRETTY_PRINT);
        $client  = new SnsClient(
            [
                'credentials' => [
                    'key'    => $this->key,
                    'secret' => $this->secret,
                ],
                'region' => 'eu-west-1',
                'version' => '2010-03-31'
            ]
        );
        try {
            $result = $client->publish(
                [
                    'Message'  => $message,
                    'TopicArn' => 'arn:aws:sns:eu-west-1:218610432265:UsersCreated',
                ]
            );
            var_dump($result);
        } catch (AwsException $e) {
            // output error message if fails
            var_dump($e->getMessage());
        }
    }

}
