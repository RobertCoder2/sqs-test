### Config database in files:

`.env.local` -> DATABASE_PASSWORD=

`config/packages/doctrine.yaml`

### Config AWS keys in files:
`src/Command/AddToQueueCommand.php` -> $key, $secret

`src/Command/ReadMessageCommand.php` -> $key, $secret


### Run composer
`composer install`

### Commands
`php bin/console app:add-to-queue`

`php bin/console app:read-message`